#ifndef GAME_OBJECT
#define GAME_0BJECT
#include <string>
 
using namespace std;
 
 class Gameobject {

 private:
 int pos_x;
 int pos_y;
 char sprite;
 public:
  
 Gameobject();
 ~Gameobject();

 int getPos_x();
 void setPos_x(int position_x);
 
 int getPos_y();
 void setPos_y(int position_y);

 char getSprite();
 void setSprite(char sprite);
 


};
#endif
