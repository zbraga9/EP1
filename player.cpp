#include <iostream>
#include <string>
#include "player.hpp"
#include "gameobject.hpp"
#include <stdlib.h>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;

Player::Player(){}


Player::Player(char sprite, int posx, int posy){
	this->sprite = sprite;
	this->pos_x = posx;
	this->pos_y = posy;
}
  


void Player::setPos_x(int valor){

	this->pos_x += valor;

}

void Player::setPos_y(int valor){
	this->pos_y += valor;
}

void Player::setSprite(char sprite){
	this->sprite = sprite;
}

int Player::getPos_x(){
	return this->pos_x;
}

int Player::getPos_y(){
	return this->pos_y;
}

char Player::getSprite(){
	return this->sprite;
}


void Player::movimento(){

	char direcao = 'l';

        
	direcao = getch();
        
        
  
	if(direcao == 'w'){
		this->setPos_y(-1);
	} else if (direcao == 's'){
		this->setPos_y(1);
	} else if (direcao == 'a'){
		this->setPos_x(-1);
	} else if (direcao == 'd'){
		this->setPos_x(1);
                
}

                   
}

