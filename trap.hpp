#ifndef TRAP_HPP
#define TRAP_HPP
#include <string>

using namespace std;

class Trap{
private:
 int damage;
 int pos_x;
 int pos_y;
 char sprite;
public:
Trap();
Trap(char sprite, int posx, int posy);
~Trap();

int getDamage();
int getPos_x();
int getPos_y();
char getSprite();
 
 
 void setDamage(int damage);
 void setPos_x(int pos_x);
 void setPos_y(int pos_y);
 void setSprite(char sprite);



};
#endif
