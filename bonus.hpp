#ifndef BONUS_HPP
#define BONUS_HPP
#include <string>

using namespace std;

class Bonus{
        private:
                char sprite;
                int pos_x;
                int pos_y;
                int score;

        public:
                Bonus();
                Bonus(char sprite, int posx, int posy);
                ~Bonus();
               

                int getScore();
                void setScore(int score);
                void setPos_x(int pos_x);
                void setPos_y(int pos_y);
                void setSprite(char sprite);
                int getPos_x();
                int getPos_y();
                char getSprite();



};
#endif

