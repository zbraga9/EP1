#ifndef MAP_HPP
#define MAP_HPP

#include <iostream>
#include <string>

class Map{

	private:
		char range[20][50];

	public:
		Map();
               ~Map();
		
		void setRange();
		void getRange();
		void addElemento(char sprite, int posx, int posy);

};

#endif
