#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <iostream>
#include <string>

class Player{

	private:
		char sprite;
	        bool winner;
                bool alive;
                int pos_x;
                int pos_y;
                int hp;

	public:
		Player();
		Player(char sprite, int posx, int posy);
                
                int getHp();
                void setHp(int hp);
                bool getAlive();
                void setAlive(bool alive);
                bool getWinner();
                void setWin(bool winner);
		void setPos_x(int valor);
		void setPos_y(int valor);
		void setSprite(char sprite);
		int getPos_x();
		int getPos_y();
		char getSprite();
		void movimento();

};

#endif
